interface User {
    rut: string;
    name: string;
}
export declare class MongoData {
    mongoService: any;
    constructor(MongoService: any);
    getUser(rut: string): Promise<User>;
    createUser(user: User): Promise<User>;
}
export {};
