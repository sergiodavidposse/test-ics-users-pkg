"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UCFindUser = void 0;
var User_1 = require("../User");
var UCFindUser = /** @class */ (function () {
    function UCFindUser() {
        var _this = this;
        this.findUser = function (user) {
            return _this.userModel.getUser(user);
        };
        this.userModel = new User_1.UserModel();
    }
    return UCFindUser;
}());
exports.UCFindUser = UCFindUser;
//# sourceMappingURL=FindUser.js.map