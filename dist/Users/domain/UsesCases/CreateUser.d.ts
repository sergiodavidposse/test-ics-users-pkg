import type { UserEntity } from '../User';
import { InterfaceUserModel } from '../User';
export interface InterfaceUCCreateUser {
    createUser: (user: UserEntity) => UserEntity | boolean;
}
export declare class UCCreateUser implements InterfaceUCCreateUser {
    protected userModel: InterfaceUserModel;
    constructor();
    createUser: (user: UserEntity) => UserEntity | boolean;
}
