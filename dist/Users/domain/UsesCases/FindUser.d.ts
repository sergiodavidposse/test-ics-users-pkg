import type { UserEntity } from '../User';
import { InterfaceUserModel } from '../User';
export interface InterfaceUCFindUser {
    findUser: (user: UserEntity) => UserEntity | boolean;
}
export declare class UCFindUser implements InterfaceUCFindUser {
    protected userModel: InterfaceUserModel;
    constructor();
    findUser: (user: UserEntity) => UserEntity | boolean;
}
