"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UCCreateUser = void 0;
var User_1 = require("../User");
var UCCreateUser = /** @class */ (function () {
    function UCCreateUser() {
        var _this = this;
        this.createUser = function (user) {
            return _this.userModel.create(user);
        };
        this.userModel = new User_1.UserModel();
    }
    return UCCreateUser;
}());
exports.UCCreateUser = UCCreateUser;
//# sourceMappingURL=CreateUser.js.map