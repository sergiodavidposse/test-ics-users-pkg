"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserService = void 0;
var CreateUser_1 = require("../UsesCases/CreateUser");
var FindUser_1 = require("../UsesCases/FindUser");
var data_1 = require("../../data");
var UserService = /** @class */ (function () {
    function UserService(FetchService, MongoService) {
        if (MongoService) {
            this.mongoRepository = new data_1.MongoData(MongoService);
        }
        this.uCCreateUser = new CreateUser_1.UCCreateUser();
        this.uCFindUser = new FindUser_1.UCFindUser();
    }
    UserService.prototype.createUser = function (rut, name) {
        return __awaiter(this, void 0, void 0, function () {
            var mongoUser, newUser, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 4, , 5]);
                        if (!this.mongoRepository) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.mongoRepository.createUser({ name: name, rut: rut })];
                    case 1:
                        mongoUser = _a.sent();
                        newUser = this.uCCreateUser.createUser(mongoUser);
                        if (typeof newUser === 'boolean') {
                            throw new Error('UCCreateUser return false');
                        }
                        return [2 /*return*/, newUser];
                    case 2: throw new Error('Es necesario inyectar Mongo');
                    case 3: return [3 /*break*/, 5];
                    case 4:
                        e_1 = _a.sent();
                        throw new Error('No se pudo crear usuario');
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    UserService.prototype.getUser = function (rut) {
        return __awaiter(this, void 0, void 0, function () {
            var userMongo, user_1, er_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 4, , 5]);
                        if (!this.mongoRepository) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.mongoRepository.getUser(rut)];
                    case 1:
                        userMongo = _a.sent();
                        user_1 = this.uCFindUser.findUser(userMongo);
                        if (typeof user_1 === 'boolean') {
                            throw new Error('No se pudo crear el usuario');
                        }
                        return [2 /*return*/, new Promise(function (resolve, reject) {
                                setTimeout(function () {
                                    resolve(user_1);
                                }, 1000);
                            })];
                    case 2: throw new Error('Es necesario inyectar Mongo');
                    case 3: return [3 /*break*/, 5];
                    case 4:
                        er_1 = _a.sent();
                        return [2 /*return*/, "Excepcion al obtener usuario: " + er_1];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    return UserService;
}());
exports.UserService = UserService;
//# sourceMappingURL=UsersController.js.map