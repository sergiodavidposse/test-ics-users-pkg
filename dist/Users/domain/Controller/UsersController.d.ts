import type { UserEntity } from '../User';
import { InterfaceUCCreateUser } from '../UsesCases/CreateUser';
import { InterfaceUCFindUser } from '../UsesCases/FindUser';
export interface InterfaceUserService {
    createUser: (rut: string, name: string) => Promise<UserEntity>;
    getUser: (rut: string) => Promise<UserEntity | string>;
}
export declare class UserService implements InterfaceUserService {
    mongoRepository: any;
    protected uCCreateUser: InterfaceUCCreateUser;
    protected uCFindUser: InterfaceUCFindUser;
    constructor(FetchService: any, MongoService: any);
    createUser(rut: string, name: string): Promise<UserEntity>;
    getUser(rut: string): Promise<UserEntity | string>;
}
