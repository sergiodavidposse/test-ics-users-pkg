import type { UserEntity } from './UserEntity';
export interface InterfaceUserModel {
    create: (user: UserEntity) => UserEntity | boolean;
    getUser: (user: UserEntity) => UserEntity;
}
export declare class UserModel implements InterfaceUserModel {
    create: (user: UserEntity) => false | UserEntity;
    getUser: (user: UserEntity) => UserEntity;
}
