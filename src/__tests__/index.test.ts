import { UserService } from '../index';
import MongoAdapter from '../../infra/persistence/mongo';

jest.setTimeout(15000);

let response: any;
let usersService = new UserService(null, MongoAdapter);

beforeEach(async () => {
  response = await usersService.createUser('150356873', 'pedro');
});

describe('Testing UserService>createUser', () => {
  it('Response debe estar definida', async () => {
    console.log(response);
    expect(response).toBeDefined();
  });
});
