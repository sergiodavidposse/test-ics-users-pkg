interface User {
  rut: string;
  name: string;
}

export class MongoData {
  mongoService: any;

  constructor(MongoService: any) {
    this.mongoService = new MongoService();
    console.log(this.mongoService.getConnection());
  }

  getUser(rut: string): Promise<User> {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(this.mongoService.find(rut));
      }, 1000);
    });
  }

  async createUser(user: User): Promise<User> {
    let newUser = await this.mongoService.save(user);
    return newUser;
  }
}
