import type { UserEntity } from '../User';
import { InterfaceUCCreateUser, UCCreateUser } from '../UsesCases/CreateUser';
import { InterfaceUCFindUser, UCFindUser } from '../UsesCases/FindUser';
import { MongoData } from '../../data';

export interface InterfaceUserService {
  createUser: (rut: string, name: string) => Promise<UserEntity>;
  getUser: (rut: string) => Promise<UserEntity | string>;
}

export class UserService implements InterfaceUserService {
  mongoRepository: any;
  protected uCCreateUser: InterfaceUCCreateUser;
  protected uCFindUser: InterfaceUCFindUser;

  constructor(FetchService: any, MongoService: any) {
    if (MongoService) {
      this.mongoRepository = new MongoData(MongoService);
    }
    this.uCCreateUser = new UCCreateUser();
    this.uCFindUser = new UCFindUser();
  }

  async createUser(rut: string, name: string): Promise<UserEntity> {
    try {
      if (this.mongoRepository) {
        const mongoUser = await this.mongoRepository.createUser({ name, rut });
        const newUser = this.uCCreateUser.createUser(mongoUser);
        if (typeof newUser === 'boolean') {
          throw new Error('UCCreateUser return false');
        }
        return newUser;
      } else {
        throw new Error('Es necesario inyectar Mongo');
      }
    } catch (e) {
      throw new Error('No se pudo crear usuario');
    }
  }

  async getUser(rut: string): Promise<UserEntity | string> {
    try {
      if (this.mongoRepository) {
        const userMongo = await this.mongoRepository.getUser(rut);
        const user = this.uCFindUser.findUser(userMongo);
        if (typeof user === 'boolean') {
          throw new Error('No se pudo crear el usuario');
        }
        return new Promise((resolve, reject) => {
          setTimeout(() => {
            resolve(user);
          }, 1000);
        });
      } else {
        throw new Error('Es necesario inyectar Mongo');
      }
    } catch (er) {
      return `Excepcion al obtener usuario: ${er}`;
    }
  }
}
