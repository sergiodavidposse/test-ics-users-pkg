export type UserEntity = {
  rut: string;
  name: string;
};
