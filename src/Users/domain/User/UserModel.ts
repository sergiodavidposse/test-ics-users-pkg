import type { UserEntity } from './UserEntity';

export interface InterfaceUserModel {
  create: (user: UserEntity) => UserEntity | boolean;
  getUser: (user: UserEntity) => UserEntity;
}

export class UserModel implements InterfaceUserModel {
  create = (user: UserEntity) => (user.rut ? user : false);
  getUser = (user: UserEntity) => user;
}
