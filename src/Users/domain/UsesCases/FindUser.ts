import type { UserEntity } from '../User';
import { InterfaceUserModel, UserModel } from '../User';

export interface InterfaceUCFindUser {
  findUser: (user: UserEntity) => UserEntity | boolean;
}

export class UCFindUser implements InterfaceUCFindUser {
  protected userModel: InterfaceUserModel;
  constructor() {
    this.userModel = new UserModel();
  }
  findUser = (user: UserEntity): UserEntity | boolean => {
    return this.userModel.getUser(user);
  };
}
