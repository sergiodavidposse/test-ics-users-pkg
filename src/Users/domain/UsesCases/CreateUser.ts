import type { UserEntity } from '../User';
import { InterfaceUserModel, UserModel } from '../User';

export interface InterfaceUCCreateUser {
  createUser: (user: UserEntity) => UserEntity | boolean;
}

export class UCCreateUser implements InterfaceUCCreateUser {
  protected userModel: InterfaceUserModel;
  constructor() {
    this.userModel = new UserModel();
  }
  createUser = (user: UserEntity): UserEntity | boolean => {
    return this.userModel.create(user);
  };
}
