import * as data from './data';
import * as domain from './domain';

export { data, domain };
