import axios from 'axios';
import { SALES_ORDERS_SERVICE_URL, SALES_ORDERS_SERVICE_KEY } from './config';

export class AdapterFetch {
  protected configAxios: any;
  configParams: any;
  constructor() {
    this.configAxios = (baseUrl: any) => {
      let instance = axios.create({
        baseURL: baseUrl,
      });
      return instance;
    };
    this.configParams = {
      params: {
        'x-api-key': SALES_ORDERS_SERVICE_KEY,
      },
    };
  }

  async listSalesOrders(filter: any) {
    let instance = this.configAxios(SALES_ORDERS_SERVICE_URL);
    const body = {
      query:
        'query getOrdersPagination($filter: OrderInput!) {\n\tordersPagination(\n filters: $filter\n ) {\n data {\n order_number\n invoice_type\n origin_completed_at\n origin_order_number\n payments {\n method_name\n }\n total_gross_price\n total_net_price\n billing_info {\n folio\n external_order_number\n transaccion\n terminal\n dte_url\n }\n customer {\n first_name\n last_name\n identity_document_number\n email\n phone_number\n }\n order_items {\n sku\n title\n gross_price\n external_order_line_number\n delivery_date\n price_after_discounts\n seller_id\n quantity\n options {\n label\n id\n }\n attributes {\n name\n value\n }\n }\n total_gross_price\n shipments {\n external_order_number\n }\n }\n pagination {\n page\n size\n totalCount\n totalPage\n }\n }\n}',
      variables: {
        filter,
      },
    };
    return await instance.post('', body, this.configParams);
  }
}
