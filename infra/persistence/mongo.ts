interface User {
  rut: string;
  name: string;
}

export default class MongoAdapter {
  constructor() {}
  getConnection() {
    return 'mongodb string connection....';
  }
  find(rut: string): Promise<User> {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve({
          rut: '150356873',
          name: 'Juan',
        });
      }, 1000);
    });
  }
  save(user: User): Promise<User> {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(user);
      }, 1000);
    });
  }
}
